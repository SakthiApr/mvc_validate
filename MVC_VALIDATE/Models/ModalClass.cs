﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_VALIDATE.Models
{
    public class ModalClass
    {
        [Required(ErrorMessage = "Please enter name.")]
        public string EmpName { get; set; }
        [Required(ErrorMessage = "Please enter email address.")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter age.")]
        public int Age { get; set; }
           [Required(ErrorMessage = "Please select gender.")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Please select country.")]
        public string CountryId { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        // [Required(ErrorMessage = "Please select atleast one course.")]
        public int CourseId { get; set; }
        public IList<SelectListItem> CourseList { get; set; }
        public string Course { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Please select atleast one course.")]
        public bool Checked { get; set; }
        public DataTable Customer { get; set; }
    }

    public class ValidationClass
    {
      
        public string EmpName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public string CountryId { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public int CourseId { get; set; }
        public IList<SelectListItem> CourseList { get; set; }
        public string Course { get; set; }
        public bool Checked { get; set; }
        public DataTable Customer { get; set; }
    }
    public enum Country
    {
        India=1,Uk=2,USA=3,China=4
    }



}