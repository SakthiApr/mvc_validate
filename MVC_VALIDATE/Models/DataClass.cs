﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MVC_VALIDATE.Models
{
    public class DataClass
    {
        public string conStr = ConfigurationManager.ConnectionStrings["connectString"].ToString();

        public DataTable GetCustomers()
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand("",con);
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
           return dt;
        }
        public int InsertCustomer()
        {
            int i = 0;
            try
            {
                SqlConnection con = new SqlConnection(conStr);
                SqlCommand cmd = new SqlCommand("", con);
                 i = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                i = 0;
            }
            return i;
        }

    }
}