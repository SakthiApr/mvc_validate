﻿using MVC_VALIDATE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_VALIDATE.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            ModalClass model = new ModalClass();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "--Select--", Value = "" });
            items.Add(new SelectListItem { Text = "India", Value = "0" });

            items.Add(new SelectListItem { Text = "China", Value = "1" });

            items.Add(new SelectListItem { Text = "UK", Value = "2" });

            //, Selected = true

            items.Add(new SelectListItem { Text = "USA", Value = "3" });

            model.CountryList = items;

            List<SelectListItem> courses = new List<SelectListItem>();
            

            courses.Add(new SelectListItem { Text = ".Net", Value = "1" , Selected=false });

            courses.Add(new SelectListItem { Text = "PHP", Value = "2", Selected = false });

            //, Selected = true

            courses.Add(new SelectListItem { Text = "Android", Value = "3", Selected = false });
            courses.Add(new SelectListItem { Text = "Java", Value = "4", Selected = false });

            model.CourseList = courses;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ModalClass Obj)
        {
            for (int i = 0; i < Obj.CourseList.Count; i++)
            {
                if (Convert.ToBoolean(Obj.CourseList[i].Selected) == true)
                {

                    Obj.Checked = true;
                    break;
                }
                else
                {
                    Obj.Checked = false;
                }

            }

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "--Select--", Value = "" });
            items.Add(new SelectListItem { Text = "India", Value = "0" });

            items.Add(new SelectListItem { Text = "China", Value = "1" });

            items.Add(new SelectListItem { Text = "UK", Value = "2" });

            //, Selected = true

            items.Add(new SelectListItem { Text = "USA", Value = "3" });

            Obj.CountryList = items;
            List<SelectListItem> courses = new List<SelectListItem>();


            courses.Add(new SelectListItem { Text = ".Net", Value = "1", Selected = false });

            courses.Add(new SelectListItem { Text = "PHP", Value = "2", Selected = false });

            //, Selected = true

            courses.Add(new SelectListItem { Text = "Android", Value = "3", Selected = false });
            courses.Add(new SelectListItem { Text = "Java", Value = "4", Selected = false });

            Obj.CourseList = courses;
            if (ModelState.IsValid)
            {
                return View(Obj);
            }
            else
            {
                return View(Obj);
            }
        }

        public ActionResult ViewAll()
        {
            return View();
        }

        public ActionResult ClientValidation()
        {
            return View();
        }

    }
}
